package com.hcl.tradingapplication.util;

public interface SuccessResponse {
	String SUCCESS_CODE1 = "2001";
	String SUCCESS_CODE2 = "2002";
	String SUCCESS_MESSAGE1 = "Cash balance updated successfully";
	String SUCCESS_MESSAGE2 = "Shares purchased successfully";
}
