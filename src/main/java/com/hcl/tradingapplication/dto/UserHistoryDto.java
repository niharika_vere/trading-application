package com.hcl.tradingapplication.dto;

import java.util.List;

import lombok.Builder;

@Builder
public record UserHistoryDto(Double cashBalance,List<OrderHistoryDto> orderHistoryDtos ,List<TradeHistoryDto> tradeHistoryDtos) {
 
}
