package com.hcl.tradingapplication.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Builder;

@Builder
public record TransactionDto(Long userId, Long stockId,
		@Min(value = 1, message = "minimum noOfShares must be 1") @Max(value = 10, message = "maximum limit to buy shares is 10") Integer noOfShares) {

}
