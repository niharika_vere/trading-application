package com.hcl.tradingapplication.dto;

import lombok.Builder;

@Builder
public record ApiResponse(String message,String httpStatus) {

}
