package com.hcl.tradingapplication.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Builder;

@Builder
public record CashAccountRecord(Long cashAccountId,
		@Min(value = 1, message = "minimum balance to transfer is 1") @Max(value = 5000, message = "Maximum balance to transfer limit is 5000 ") Double balance) {

}
