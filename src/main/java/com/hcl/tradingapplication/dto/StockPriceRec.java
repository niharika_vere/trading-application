package com.hcl.tradingapplication.dto;

import java.time.LocalDate;

import lombok.Builder;
@Builder
public record StockPriceRec(String stockName, Double Price, LocalDate date) {

}
