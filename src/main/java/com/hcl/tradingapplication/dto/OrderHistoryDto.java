package com.hcl.tradingapplication.dto;

import com.hcl.tradingapplication.entity.Status;

import lombok.Builder;

@Builder
public record OrderHistoryDto(String CompanyName, Integer shares, Status status, Double totalCost) {

}
