package com.hcl.tradingapplication.exception;

public class TransactionHistoryNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TransactionHistoryNotFound(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public TransactionHistoryNotFound() {
		super("Transaction History Not Found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
