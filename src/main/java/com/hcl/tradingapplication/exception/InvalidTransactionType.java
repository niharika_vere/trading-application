package com.hcl.tradingapplication.exception;

public class InvalidTransactionType extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidTransactionType(String message) {
		super(message, GlobalErrorCode.ERROR_BAD_REQUEST);
	}

	public InvalidTransactionType() {
		super("Invalid Transaction Type", GlobalErrorCode.ERROR_BAD_REQUEST);
	}
}
