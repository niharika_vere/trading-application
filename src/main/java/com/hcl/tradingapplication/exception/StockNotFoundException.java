package com.hcl.tradingapplication.exception;

public class StockNotFoundException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public StockNotFoundException(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public StockNotFoundException() {
		super("Stock Not Found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
