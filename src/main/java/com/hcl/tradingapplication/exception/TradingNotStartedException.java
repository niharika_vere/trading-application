package com.hcl.tradingapplication.exception;

public class TradingNotStartedException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TradingNotStartedException(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public TradingNotStartedException() {
		super("Trading not started", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
