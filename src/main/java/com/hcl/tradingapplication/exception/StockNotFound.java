package com.hcl.tradingapplication.exception;

public class StockNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public StockNotFound(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public StockNotFound() {
		super("Stock Not Found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
