package com.hcl.tradingapplication.exception;

public class InsufficientCashBalance extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InsufficientCashBalance(String message) {
		super(message, GlobalErrorCode.ERROR_BAD_REQUEST);
	}

	public InsufficientCashBalance() {
		super("Insufficient Cash Balance", GlobalErrorCode.ERROR_BAD_REQUEST);
	}
}
