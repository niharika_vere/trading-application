package com.hcl.tradingapplication.service;

import com.hcl.tradingapplication.dto.ApiResponse;
import com.hcl.tradingapplication.dto.CashAccountRecord;

public interface CashAccountSerevice {

	ApiResponse updateBalance(CashAccountRecord cashAccountRecord);

}
