package com.hcl.tradingapplication.service;

import com.hcl.tradingapplication.dto.UserHistoryDto;

public interface UserTradingHistoryService {

	UserHistoryDto tradingHistory(Long userId);

}
