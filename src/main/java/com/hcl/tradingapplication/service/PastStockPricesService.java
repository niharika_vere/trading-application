package com.hcl.tradingapplication.service;

import java.util.List;

import com.hcl.tradingapplication.dto.StockPriceRec;

public interface PastStockPricesService {

	List<StockPriceRec> getStockPrices(Long stockId);

}
