package com.hcl.tradingapplication.service.impl;

import java.time.LocalDate;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.tradingapplication.dto.ApiResponse;
import com.hcl.tradingapplication.dto.TransactionDto;
import com.hcl.tradingapplication.entity.CashAccount;
import com.hcl.tradingapplication.entity.Status;
import com.hcl.tradingapplication.entity.Stock;
import com.hcl.tradingapplication.entity.StockPrice;
import com.hcl.tradingapplication.entity.Trade;
import com.hcl.tradingapplication.entity.Transaction;
import com.hcl.tradingapplication.entity.TransactionType;
import com.hcl.tradingapplication.entity.User;
import com.hcl.tradingapplication.exception.InsufficientCashBalance;
import com.hcl.tradingapplication.exception.InvalidTransactionType;
import com.hcl.tradingapplication.exception.StockNotFound;
import com.hcl.tradingapplication.exception.UserNotFound;
import com.hcl.tradingapplication.repository.CashAccountRepository;
import com.hcl.tradingapplication.repository.StockPriceRepository;
import com.hcl.tradingapplication.repository.StockRepository;
import com.hcl.tradingapplication.repository.TradeRepository;
import com.hcl.tradingapplication.repository.TransactionRepository;
import com.hcl.tradingapplication.repository.UserRepository;
import com.hcl.tradingapplication.service.TransactionService;
import com.hcl.tradingapplication.util.SuccessResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransactionServiceImpl implements TransactionService {

	private final UserRepository userRepository;
	private final StockRepository stockRepository;
	private final StockPriceRepository stockPriceRepository;
	private final TradeRepository tradeRepository;
	private final TransactionRepository transactionRepository;
	private final CashAccountRepository cashAccountRepository;

	@Override
	@Transactional
	public ApiResponse buyShares(TransactionType transactionType, TransactionDto transactionDto) {

		if (transactionType.equals(TransactionType.SELL))
			throw new InvalidTransactionType();
		User user = userRepository.findById(transactionDto.userId()).orElseThrow(() -> {
			log.error("invalid userId caused UserNotFoundException");
			throw new UserNotFound();
		});

		Stock stock = stockRepository.findById(transactionDto.stockId()).orElseThrow(() -> {
			log.error("invalid stockId caused StockNotFoundException");
			throw new StockNotFound();
		});

		StockPrice stockPrice = stockPriceRepository.findByStockAndDate(stock, LocalDate.now());

		CashAccount cashAccount = cashAccountRepository.findByUser(user);

		Double totalPrice = transactionDto.noOfShares() * stockPrice.getPrice();
		Transaction transaction = Transaction.builder().noOfUnits(transactionDto.noOfShares()).stock(stock)
				.totalCost(totalPrice).transactionDate(LocalDate.now()).transactionType(transactionType).user(user)
				.build();
		if (cashAccount.getBalance() > totalPrice) {
			transaction.setStatus(Status.SUCCESS);
			log.info("transaction saved" + transaction);
			transactionRepository.save(transaction);
			Trade trade = Trade.builder().buyDate(LocalDate.now()).stock(stock).transaction(transaction).user(user)
					.build();
			log.info("trade saved" + trade);
			tradeRepository.save(trade);
			cashAccount.setBalance(cashAccount.getBalance() - totalPrice);
			log.info("cash balance updated" + cashAccount);
			cashAccountRepository.save(cashAccount);
		} else {
			transaction.setStatus(Status.FAILED);
			transactionRepository.save(transaction);
			log.error("Insufficient Funds");
			throw new InsufficientCashBalance(String.format(
					"Insufficient Cash Balance Possible Try Buying less Than:%d shares", transactionDto.noOfShares()));
		}
		return ApiResponse.builder().message(SuccessResponse.SUCCESS_MESSAGE2).httpStatus(SuccessResponse.SUCCESS_CODE2)
				.build();
	}

}
