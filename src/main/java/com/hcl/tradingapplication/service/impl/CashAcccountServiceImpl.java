package com.hcl.tradingapplication.service.impl;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.hcl.tradingapplication.dto.ApiResponse;
import com.hcl.tradingapplication.dto.CashAccountRecord;
import com.hcl.tradingapplication.entity.CashAccount;
import com.hcl.tradingapplication.exception.UserNotFound;
import com.hcl.tradingapplication.repository.CashAccountRepository;
import com.hcl.tradingapplication.service.CashAccountSerevice;
import com.hcl.tradingapplication.util.SuccessResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class CashAcccountServiceImpl implements CashAccountSerevice {

	private final CashAccountRepository cashAccountRepository;

	/**
	 * 
	 * 
	 * Updating Account balance using AccountId
	 * 
	 * @param cashAccountRecord ask account and balance
	 * @return Balance updated
	 * 
	 */

	@Override
	public ApiResponse updateBalance(CashAccountRecord cashAccountRecord) {
		CashAccount cashAccount = cashAccountRepository.findById(cashAccountRecord.cashAccountId()).orElseThrow(() -> {
			log.error("Invalid AccountId found");

			throw new UserNotFound("AccountId not found");
		});

		cashAccount.setBalance(cashAccount.getBalance() + cashAccountRecord.balance());
		cashAccount.setLastModifiedOn(LocalDate.now());
		log.info("Accountbalance update saved" + cashAccount);
		cashAccountRepository.save(cashAccount);
		return ApiResponse.builder().message(SuccessResponse.SUCCESS_MESSAGE1).httpStatus(SuccessResponse.SUCCESS_CODE1)
				.build();

	}
}
