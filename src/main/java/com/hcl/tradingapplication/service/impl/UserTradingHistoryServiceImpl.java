package com.hcl.tradingapplication.service.impl;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.hcl.tradingapplication.dto.OrderHistoryDto;
import com.hcl.tradingapplication.dto.TradeHistoryDto;
import com.hcl.tradingapplication.dto.UserHistoryDto;
import com.hcl.tradingapplication.entity.CashAccount;
import com.hcl.tradingapplication.entity.Stock;
import com.hcl.tradingapplication.entity.Trade;
import com.hcl.tradingapplication.entity.Transaction;
import com.hcl.tradingapplication.entity.User;
import com.hcl.tradingapplication.exception.TransactionHistoryNotFound;
import com.hcl.tradingapplication.exception.UserNotFound;
import com.hcl.tradingapplication.repository.CashAccountRepository;
import com.hcl.tradingapplication.repository.TradeRepository;
import com.hcl.tradingapplication.repository.TransactionRepository;
import com.hcl.tradingapplication.repository.UserRepository;
import com.hcl.tradingapplication.service.UserTradingHistoryService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserTradingHistoryServiceImpl implements UserTradingHistoryService {
	private final UserRepository userRepository;
	private final TransactionRepository transactionRepository;
	private final CashAccountRepository cashAccountRepository;
	private final TradeRepository tradeRepository;

	@Override
	public UserHistoryDto tradingHistory(Long userId) {
		User user = userRepository.findById(userId).orElseThrow(UserNotFound::new);
		List<Transaction> transactions = transactionRepository.findByUser(user);
		if (transactions.isEmpty()) {
			log.warn("Transaction History Not Found");
			throw new TransactionHistoryNotFound();
		}
		Map<Transaction, Stock> transactionMap = transactions.stream()
				.collect(Collectors.toMap(Function.identity(), Transaction::getStock));

		List<OrderHistoryDto> orderHistoryDtos = transactions.stream().map(stocksTransaction -> {
			Stock stock = transactionMap.get(stocksTransaction);
			return new OrderHistoryDto(stock.getCompany(), stocksTransaction.getNoOfUnits(),
					stocksTransaction.getStatus(), stocksTransaction.getTotalCost());

		}).toList();

		CashAccount cashBlance = cashAccountRepository.findByUser(user);
		List<Trade> trades = tradeRepository.findByUser(user);

		Map<Trade, Stock> tradeMap = trades.stream().collect(Collectors.toMap(Function.identity(), Trade::getStock));

		List<TradeHistoryDto> tradeHistoryDtos = trades.stream().map(trade -> {
			Stock stock = tradeMap.get(trade);
			return new TradeHistoryDto(stock.getCompany(), stock.getBseCode(), stock.getPe(), stock.getMarketCap(),
					stock.getChangePct());

		}).toList();

		log.info("Fetch User History");
		return new UserHistoryDto(cashBlance.getBalance(), orderHistoryDtos, tradeHistoryDtos);
	}
}
