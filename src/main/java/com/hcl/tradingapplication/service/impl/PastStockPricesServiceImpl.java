package com.hcl.tradingapplication.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.tradingapplication.dto.StockPriceRec;
import com.hcl.tradingapplication.entity.Stock;
import com.hcl.tradingapplication.entity.StockPrice;
import com.hcl.tradingapplication.exception.StockNotFoundException;
import com.hcl.tradingapplication.exception.TradingNotStartedException;
import com.hcl.tradingapplication.repository.StockPriceRepository;
import com.hcl.tradingapplication.repository.StockRepository;
import com.hcl.tradingapplication.service.PastStockPricesService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class PastStockPricesServiceImpl implements PastStockPricesService{
	private final StockRepository stockRepository;
	private final StockPriceRepository stockPriceRepository;
	/**
	 * 
	* Getting last 5 days stock price details by using StockId
	* @param stockId will give the stock details
	* @return getting all list of prices of stocks of last five days
	*/
	@Override
	public List<StockPriceRec> getStockPrices(Long stockId) {
		log.warn("Stock not found");
		Stock stock = stockRepository.findById(stockId).orElseThrow( StockNotFoundException::new);
		String name = stock.getCompany();
		List<StockPrice> stockPrice = stockPriceRepository.findTop5ByStockOrderByDateDesc(stock);
		if(stockPrice.isEmpty()) {
			log.warn("Trading not started");
			throw new  TradingNotStartedException();
		}
		log.info("Details fetched successfully");
		return stockPrice.stream()
		.map(stockPric -> new StockPriceRec(name,stockPric.getPrice(),stockPric.getDate())).toList();
		
	}

}
