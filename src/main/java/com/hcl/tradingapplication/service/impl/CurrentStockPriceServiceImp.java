package com.hcl.tradingapplication.service.impl;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.hcl.tradingapplication.dto.StockPriceRec;
import com.hcl.tradingapplication.entity.Stock;
import com.hcl.tradingapplication.entity.StockPrice;
import com.hcl.tradingapplication.exception.StockNotFoundException;
import com.hcl.tradingapplication.exception.TradingNotStartedException;
import com.hcl.tradingapplication.repository.StockPriceRepository;
import com.hcl.tradingapplication.repository.StockRepository;
import com.hcl.tradingapplication.service.CurrentStockPriceService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Service
@RequiredArgsConstructor
@Slf4j
public class CurrentStockPriceServiceImp implements CurrentStockPriceService{
	
	private final StockRepository stockRepository;
	private final StockPriceRepository stockPriceRepository;
	/**
	 * 
	* Getting Current date stock price with name by using stockId
	* @param stockId will give the stock details
	* @return getting price of current date
	* 
	*/
	
	@Override
	public StockPriceRec getCurrentPriceStock(Long stockId) {
		Stock stock = stockRepository.findById(stockId).orElseThrow(StockNotFoundException::new);
		Optional<StockPrice> stockPrice = stockPriceRepository.findFirstByStockOrderByDateDesc(stock);
		String name = stock.getCompany();	
		if(stockPrice.isEmpty()) {
			log.warn("Trading not started");
			throw new  TradingNotStartedException();
		}
		StockPrice price = stockPrice.get();
		return new StockPriceRec(name, price.getPrice(),price.getDate());
	}
 
}
