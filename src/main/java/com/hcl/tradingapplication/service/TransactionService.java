package com.hcl.tradingapplication.service;

import com.hcl.tradingapplication.dto.ApiResponse;
import com.hcl.tradingapplication.dto.TransactionDto;
import com.hcl.tradingapplication.entity.TransactionType;

public interface TransactionService {

	ApiResponse buyShares(TransactionType transactionType, TransactionDto transactionDto);

}
