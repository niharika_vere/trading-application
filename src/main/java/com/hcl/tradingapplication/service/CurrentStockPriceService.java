package com.hcl.tradingapplication.service;

import com.hcl.tradingapplication.dto.StockPriceRec;
import com.hcl.tradingapplication.entity.StockPrice;

public interface CurrentStockPriceService {

	StockPriceRec getCurrentPriceStock(Long stockId);

}
