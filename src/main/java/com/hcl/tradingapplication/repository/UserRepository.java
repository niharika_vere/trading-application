package com.hcl.tradingapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.tradingapplication.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
