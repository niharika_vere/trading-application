package com.hcl.tradingapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.tradingapplication.entity.Stock;

public interface StockRepository extends JpaRepository<Stock, Long>{

}
