package com.hcl.tradingapplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.tradingapplication.entity.Transaction;
import com.hcl.tradingapplication.entity.User;

public interface TransactionRepository extends JpaRepository<Transaction, Long>{

	List<Transaction> findByUser(User user);

}
