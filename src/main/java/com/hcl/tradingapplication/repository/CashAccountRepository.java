package com.hcl.tradingapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.tradingapplication.entity.CashAccount;
import com.hcl.tradingapplication.entity.User;

public interface CashAccountRepository extends JpaRepository<CashAccount, Long> {
	
	CashAccount findByUser(User user);
}
