package com.hcl.tradingapplication.repository;


import java.util.List;


import java.time.LocalDate;

import java.util.Optional;


import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.tradingapplication.entity.Stock;
import com.hcl.tradingapplication.entity.StockPrice;

public interface StockPriceRepository extends JpaRepository<StockPrice, Long> {

	StockPrice findByStockAndDate(Stock stock, LocalDate localDate);


	Optional<StockPrice> findFirstByStockOrderByDateDesc(Stock stock);

	List<StockPrice> findTop5ByStockOrderByDateDesc(Stock stock);


}
