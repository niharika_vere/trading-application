package com.hcl.tradingapplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.tradingapplication.entity.Trade;
import com.hcl.tradingapplication.entity.User;

public interface TradeRepository extends JpaRepository<Trade, Long> {

	List<Trade> findByUser(User user);

}
