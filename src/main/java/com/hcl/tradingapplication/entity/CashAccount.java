package com.hcl.tradingapplication.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class CashAccount {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cashAccountId;
	
	private Double balance;
	
	@OneToOne
	private User user;
	
	private LocalDate lastModifiedOn;

}
