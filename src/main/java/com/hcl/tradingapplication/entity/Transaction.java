package com.hcl.tradingapplication.entity;

import java.time.LocalDate;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Transaction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long transactionId;
	@ManyToOne
	private User user;
	private Integer noOfUnits;
	@ManyToOne
	private Stock stock;
	private Double totalCost;
	@Enumerated(EnumType.STRING)
	private Status status;
	@CreationTimestamp
	private LocalDate transactionDate;
	@Enumerated(EnumType.STRING)
	private TransactionType transactionType;
}
