package com.hcl.tradingapplication.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.tradingapplication.dto.StockPriceRec;
import com.hcl.tradingapplication.service.CurrentStockPriceService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class CurrentStockPriceController {
	private final CurrentStockPriceService currentStockPriceService;
	/**
	 * 
	* Getting Current date stock price with name by using stockId
	* @param stockId will give the stock details
	* @return getting price of current date
	* 
	*/
	@GetMapping("/stocksvalue/{stockId}")
	ResponseEntity<StockPriceRec> getCurrentPriceStock(@PathVariable Long stockId ){
		log.info("Current Stock price fetched successfully");
		return ResponseEntity.status(HttpStatus.OK).body(currentStockPriceService.getCurrentPriceStock(stockId));
	}
}
