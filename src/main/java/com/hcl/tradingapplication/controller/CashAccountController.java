package com.hcl.tradingapplication.controller;

import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.tradingapplication.dto.ApiResponse;
import com.hcl.tradingapplication.dto.CashAccountRecord;
import com.hcl.tradingapplication.service.CashAccountSerevice;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class CashAccountController {
	private final CashAccountSerevice cashAccountService;

	/**
	 * Updating Account balance using AccountId
	 * 
	 * @param cashAccountRecord ask account and balance
	 * @return Balance updated
	 */

	@PutMapping("/cash-account")
	public ResponseEntity<ApiResponse> updateBalance(@RequestBody @Valid CashAccountRecord cashAccountRecord) {
		log.info("Account balance Updated successfully");
		return ResponseEntity.status(HttpStatus.OK).body(cashAccountService.updateBalance(cashAccountRecord));

	}

}
