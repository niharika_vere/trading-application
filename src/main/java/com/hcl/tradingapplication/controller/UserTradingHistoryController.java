package com.hcl.tradingapplication.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.tradingapplication.dto.UserHistoryDto;
import com.hcl.tradingapplication.service.UserTradingHistoryService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
public class UserTradingHistoryController {
	private final UserTradingHistoryService userTradingHistoryService;
/**
 * description=Fetches the trading history of user by userId
 * @param userId
 * @return UserHistoryDto
 */
	@GetMapping("/{userId}")
	public ResponseEntity<UserHistoryDto> getUserTradingHistory(@PathVariable Long userId) {
		return ResponseEntity.status(HttpStatus.OK).body(userTradingHistoryService.tradingHistory(userId));
	}
}
