package com.hcl.tradingapplication.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.tradingapplication.dto.ApiResponse;
import com.hcl.tradingapplication.dto.TransactionDto;
import com.hcl.tradingapplication.entity.TransactionType;
import com.hcl.tradingapplication.service.TransactionService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/transactions")
@RequiredArgsConstructor
public class TransactionController {
	private final TransactionService transactionService;

	/**
	 * description=Purchase the share using stockId, noOfShares and Cash account
	 * 
	 * @param transactionType to Buy/Sell the shares
	 * @param transactionDto  to Buy shares requires Transaction details
	 * @return ApiResponse with message and status code
	 */
	@PostMapping
	public ResponseEntity<ApiResponse> buyShares(@RequestParam TransactionType transactionType,
			@RequestBody @Valid TransactionDto transactionDto) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(transactionService.buyShares(transactionType, transactionDto));
	}
}
