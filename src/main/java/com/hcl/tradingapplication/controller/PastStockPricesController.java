package com.hcl.tradingapplication.controller;


import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.tradingapplication.dto.StockPriceRec;
import com.hcl.tradingapplication.service.PastStockPricesService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class PastStockPricesController {
	private final PastStockPricesService pastStocksPriceService;
	/**
	 * 
	* Getting last 5 days stock price details by using StockId
	* @param stockId will give the stock details
	* @return getting all list of prices of stocks of last five days
	*/
		@GetMapping("stocks/{stockId}")
		ResponseEntity<List<StockPriceRec>> getStockPrice(@PathVariable Long stockId) {
			log.info("detalils of prices successfully fetched");
	    return ResponseEntity.status(HttpStatus.OK).body(pastStocksPriceService.getStockPrices(stockId));
		}
}
