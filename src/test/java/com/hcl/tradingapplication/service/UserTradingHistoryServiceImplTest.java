package com.hcl.tradingapplication.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.tradingapplication.dto.UserHistoryDto;
import com.hcl.tradingapplication.entity.CashAccount;
import com.hcl.tradingapplication.entity.Stock;
import com.hcl.tradingapplication.entity.Trade;
import com.hcl.tradingapplication.entity.Transaction;
import com.hcl.tradingapplication.entity.TransactionType;
import com.hcl.tradingapplication.entity.User;
import com.hcl.tradingapplication.exception.TransactionHistoryNotFound;
import com.hcl.tradingapplication.repository.CashAccountRepository;
import com.hcl.tradingapplication.repository.TradeRepository;
import com.hcl.tradingapplication.repository.TransactionRepository;
import com.hcl.tradingapplication.repository.UserRepository;
import com.hcl.tradingapplication.service.impl.UserTradingHistoryServiceImpl;

@ExtendWith(SpringExtension.class)
class UserTradingHistoryServiceImplTest {
	@Mock
	UserRepository userRepository;
	@Mock
	CashAccountRepository cashBlanceRepository;
	@Mock
	TransactionRepository stocksTransactionRepository;
	@Mock
	TradeRepository tradeRepository;
	@InjectMocks
	UserTradingHistoryServiceImpl userServiceImp;

	@Test
	void testSearchUserHistorySuccess() {

		User user = User.builder().userId(1L).build();
		Stock stock = Stock.builder().company("HCL Tech").bseCode(10L).changePct(10.0).marketCap(1L).pe(1D).stockId(1l)
				.build();
		Long userId = 1L;
		Transaction stocksTransaction = Transaction.builder().transactionId(1L).user(user).stock(stock).noOfUnits(10)
				.transactionType(TransactionType.BUY).totalCost(1000D).build();
		CashAccount cashBlance = CashAccount.builder().cashAccountId(1L).balance(100D).user(user).build();
		List<Transaction> stocksTransactions = Arrays.asList(stocksTransaction);
		List<Trade> trades = Arrays
				.asList(Trade.builder().stock(stock).user(user).tradeId(1L).transaction(stocksTransaction).build());

		Mockito.when(cashBlanceRepository.findByUser(user)).thenReturn(cashBlance);
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
		Mockito.when(tradeRepository.findByUser(user)).thenReturn(trades);
		Mockito.when(stocksTransactionRepository.findByUser(user)).thenReturn(stocksTransactions);

		UserHistoryDto userHistoryDto = userServiceImp.tradingHistory(userId);

		assertEquals(100D, userHistoryDto.cashBalance());
	}

	@Test
	void testSearchUserHistoryFailure() {
		User user = User.builder().userId(1L).build();
		Long userId = 1L;

		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));

		Mockito.when(stocksTransactionRepository.findByUser(user)).thenReturn(Collections.emptyList());

		assertThrows(TransactionHistoryNotFound.class, () -> userServiceImp.tradingHistory(userId));
	}
}
