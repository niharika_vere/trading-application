package com.hcl.tradingapplication.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.hcl.tradingapplication.dto.StockPriceRec;
import com.hcl.tradingapplication.entity.Stock;
import com.hcl.tradingapplication.entity.StockPrice;
import com.hcl.tradingapplication.exception.StockNotFoundException;
import com.hcl.tradingapplication.exception.TradingNotStartedException;
import com.hcl.tradingapplication.repository.StockPriceRepository;
import com.hcl.tradingapplication.repository.StockRepository;
import com.hcl.tradingapplication.service.impl.CurrentStockPriceServiceImp;

@ExtendWith(MockitoExtension.class)
 class CurrentStockPriceServiceImplTest {
 
    @Mock
    private StockRepository stockRepository;
 
    @Mock
    private StockPriceRepository stockPriceRepository;
 
    @InjectMocks
    private CurrentStockPriceServiceImp currentStockPriceService;
 
    @Test
     void testGetCurrentPriceStockSuccess() {
        Stock mockStock = new Stock(1L, "ABC Company", 123L, 10.0, 100000L, 1.0);
        StockPrice mockStockPrice = new StockPrice(1L, LocalDate.now(), mockStock, 100.0);
        when(stockRepository.findById(1L)).thenReturn(Optional.of(mockStock));
        when(stockPriceRepository.findFirstByStockOrderByDateDesc(mockStock)).thenReturn(Optional.of(mockStockPrice));
 
        StockPriceRec result = currentStockPriceService.getCurrentPriceStock(1L);
 
        assertEquals("ABC Company", result.stockName());
        assertEquals(100.0, result.Price());
        assertEquals(LocalDate.now(), result.date());
    }
 
    @Test
     void testGetCurrentPriceStockStockNotFound() {
        when(stockRepository.findById(1L)).thenReturn(Optional.empty());
 
        assertThrows(StockNotFoundException.class, () -> currentStockPriceService.getCurrentPriceStock(1L));
    }
 
    @Test
     void testGetCurrentPriceStockTradingNotStarted() {
        Stock mockStock = new Stock(1L, "ABC Company", 123L, 10.0, 100000L, 1.0);
        when(stockRepository.findById(1L)).thenReturn(Optional.of(mockStock));
        when(stockPriceRepository.findFirstByStockOrderByDateDesc(mockStock)).thenReturn(Optional.empty());
 
        assertThrows(TradingNotStartedException.class, () -> currentStockPriceService.getCurrentPriceStock(1L));
    }
    @Test
     void testGetCurrentPriceStockNullStockPrice() {
        Stock mockStock = new Stock(1L, "ABC Company", 123L, 10.0, 100000L, 1.0);
        when(stockRepository.findById(1L)).thenReturn(Optional.of(mockStock));
        when(stockPriceRepository.findFirstByStockOrderByDateDesc(mockStock)).thenReturn(Optional.empty());
     
        Exception exception = assertThrows(TradingNotStartedException.class, () -> currentStockPriceService.getCurrentPriceStock(1L));
     
        assertEquals("Trading not started", exception.getMessage());
    }
}
