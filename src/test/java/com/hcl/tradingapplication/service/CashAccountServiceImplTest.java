package com.hcl.tradingapplication.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.tradingapplication.dto.ApiResponse;
import com.hcl.tradingapplication.dto.CashAccountRecord;
import com.hcl.tradingapplication.entity.CashAccount;
import com.hcl.tradingapplication.exception.UserNotFound;
import com.hcl.tradingapplication.repository.CashAccountRepository;
import com.hcl.tradingapplication.service.impl.CashAcccountServiceImpl;

@ExtendWith(SpringExtension.class)
 class CashAccountServiceImplTest {
	@Mock
    private CashAccountRepository cashAccountRepository;
 
    @InjectMocks
    private CashAcccountServiceImpl cashAcccountServiceImpl;
 
    @Test
     void testUpdateBalanceSuccess() {
        CashAccount cashAccount = new CashAccount();
        cashAccount.setCashAccountId(1L);
        cashAccount.setBalance(100.0);
 
        CashAccountRecord cashAccountRecord = new CashAccountRecord(1L, 50.0);
 
        when(cashAccountRepository.findById(cashAccountRecord.cashAccountId()))
                .thenReturn(java.util.Optional.of(cashAccount));
 
        ApiResponse response = cashAcccountServiceImpl.updateBalance(cashAccountRecord);
 
        assertEquals("Cash balance updated successfully", response.message());
        assertEquals("2001", response.httpStatus()); 
    }
    @Test
     void testUpdateBalanceAccountNotFound() {
        CashAccountRecord cashAccountRecord = new CashAccountRecord(1L, 50.0);
 
        when(cashAccountRepository.findById(cashAccountRecord.cashAccountId()))
                .thenReturn(java.util.Optional.empty());
 
        assertThrows(UserNotFound.class, () -> cashAcccountServiceImpl.updateBalance(cashAccountRecord));
    }

}
