package com.hcl.tradingapplication.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.tradingapplication.exception.StockNotFoundException;
import com.hcl.tradingapplication.repository.StockPriceRepository;
import com.hcl.tradingapplication.repository.StockRepository;
import com.hcl.tradingapplication.service.impl.PastStockPricesServiceImpl;

@ExtendWith(SpringExtension.class)
class PastStockPricesServiceImplTest {

	@Mock
	private StockRepository stockRepository;

	@Mock
	private StockPriceRepository stockPriceRepository;

	@InjectMocks
	private PastStockPricesServiceImpl pastStockPricesService;

	@Test
	void testGetStockPricesStockNotFound() {
		Long stockId = 1L;
		Mockito.when(stockRepository.findById(stockId)).thenReturn(Optional.empty());

		assertThrows(StockNotFoundException.class, () -> pastStockPricesService.getStockPrices(stockId));
	}

}
