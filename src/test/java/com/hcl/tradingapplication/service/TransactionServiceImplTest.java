package com.hcl.tradingapplication.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.tradingapplication.dto.ApiResponse;
import com.hcl.tradingapplication.dto.TransactionDto;
import com.hcl.tradingapplication.entity.CashAccount;
import com.hcl.tradingapplication.entity.Status;
import com.hcl.tradingapplication.entity.Stock;
import com.hcl.tradingapplication.entity.StockPrice;
import com.hcl.tradingapplication.entity.Trade;
import com.hcl.tradingapplication.entity.Transaction;
import com.hcl.tradingapplication.entity.TransactionType;
import com.hcl.tradingapplication.entity.User;
import com.hcl.tradingapplication.exception.InsufficientCashBalance;
import com.hcl.tradingapplication.exception.InvalidTransactionType;
import com.hcl.tradingapplication.exception.StockNotFound;
import com.hcl.tradingapplication.exception.UserNotFound;
import com.hcl.tradingapplication.repository.CashAccountRepository;
import com.hcl.tradingapplication.repository.StockPriceRepository;
import com.hcl.tradingapplication.repository.StockRepository;
import com.hcl.tradingapplication.repository.TradeRepository;
import com.hcl.tradingapplication.repository.TransactionRepository;
import com.hcl.tradingapplication.repository.UserRepository;
import com.hcl.tradingapplication.service.impl.TransactionServiceImpl;

@ExtendWith(SpringExtension.class)
class TransactionServiceImplTest {
	@Mock
	private UserRepository userRepository;
	@Mock
	private StockRepository stockRepository;
	@Mock
	private StockPriceRepository stockPriceRepository;
	@Mock
	private TradeRepository tradeRepository;
	@Mock
	private TransactionRepository transactionRepository;
	@Mock
	private CashAccountRepository cashAccountRepository;

	@InjectMocks
	private TransactionServiceImpl transactionServiceImpl;

	@Test
	void testPurchaseShare() {
		TransactionDto transactionDto = TransactionDto.builder().noOfShares(2).stockId(1l).userId(1l).build();
		User user = User.builder().email("manoj@gmail.com").userId(1l).userName("Manoj").build();
		Stock stock = Stock.builder().bseCode(123l).changePct(2.3).company("HCL").marketCap(2345l).pe(2.2).stockId(1l)
				.build();
		StockPrice stockPrice = StockPrice.builder().date(LocalDate.now()).stock(stock).stockPriceId(1l).price(234.0)
				.build();
		CashAccount cashAccount = CashAccount.builder().balance(1200.0).cashAccountId(1l)
				.lastModifiedOn(LocalDate.now()).user(user).build();
		Transaction transaction = Transaction.builder().noOfUnits(2).status(Status.SUCCESS).stock(stock)
				.totalCost(468.0).transactionDate(LocalDate.now()).transactionId(1l)
				.transactionType(TransactionType.BUY).user(user).build();
		Trade trade = Trade.builder().buyDate(LocalDate.now()).stock(stock).tradeId(1l).transaction(transaction)
				.user(user).build();
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(user));
		Mockito.when(stockRepository.findById(transactionDto.stockId())).thenReturn(Optional.of(stock));
		Mockito.when(stockPriceRepository.findByStockAndDate(stock, LocalDate.now())).thenReturn(stockPrice);
		Mockito.when(cashAccountRepository.findByUser(user)).thenReturn(cashAccount);
		Mockito.when(transactionRepository.save(transaction)).thenReturn(transaction);
		Mockito.when(tradeRepository.save(trade)).thenReturn(trade);
		Mockito.when(cashAccountRepository.save(cashAccount)).thenReturn(cashAccount);
		ApiResponse apiResponse = transactionServiceImpl.buyShares(TransactionType.BUY, transactionDto);
		assertNotNull(apiResponse);
		assertEquals("Shares purchased successfully", apiResponse.message());
	}

	@Test
	void testPurchaseInvalid1() {
		TransactionDto transactionDto = TransactionDto.builder().noOfShares(2).stockId(1l).userId(1l).build();
		assertThrows(InvalidTransactionType.class,
				() -> transactionServiceImpl.buyShares(TransactionType.SELL, transactionDto));
	}
	
	@Test
	void testPurchaseInvalid2() {
		TransactionDto transactionDto = TransactionDto.builder().noOfShares(2).stockId(1l).userId(1l).build();
		Mockito.when(userRepository.findById(1l)).thenReturn(Optional.empty());
		assertThrows(UserNotFound.class,()->transactionServiceImpl.buyShares(TransactionType.BUY, transactionDto));
	}
	
	@Test
	void testPurchaseInvalid3() {
		TransactionDto transactionDto = TransactionDto.builder().noOfShares(2).stockId(1l).userId(1l).build();
		User user = User.builder().email("manoj@gmail.com").userId(1l).userName("Manoj").build();
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(user));
		Mockito.when(stockRepository.findById(transactionDto.stockId())).thenReturn(Optional.empty());
		assertThrows(StockNotFound.class,()->transactionServiceImpl.buyShares(TransactionType.BUY, transactionDto));
	}
	
	@Test
	void testPurchaseInvalid4() {
		TransactionDto transactionDto = TransactionDto.builder().noOfShares(2).stockId(1l).userId(1l).build();
		User user = User.builder().email("manoj@gmail.com").userId(1l).userName("Manoj").build();
		Stock stock = Stock.builder().bseCode(123l).changePct(2.3).company("HCL").marketCap(2345l).pe(2.2).stockId(1l)
				.build();
		StockPrice stockPrice = StockPrice.builder().date(LocalDate.now()).stock(stock).stockPriceId(1l).price(234.0)
				.build();
		CashAccount cashAccount = CashAccount.builder().balance(120.0).cashAccountId(1l)
				.lastModifiedOn(LocalDate.now()).user(user).build();
		Transaction transaction = Transaction.builder().noOfUnits(2).status(Status.FAILED).stock(stock).totalCost(468.0)
				.transactionDate(LocalDate.now()).transactionId(1l).transactionType(TransactionType.BUY).user(user)
				.build();
		Mockito.when(userRepository.findById(transactionDto.userId())).thenReturn(Optional.of(user));
		Mockito.when(stockRepository.findById(transactionDto.stockId())).thenReturn(Optional.of(stock));
		Mockito.when(stockPriceRepository.findByStockAndDate(stock, LocalDate.now())).thenReturn(stockPrice);
		Mockito.when(cashAccountRepository.findByUser(user)).thenReturn(cashAccount);
		Mockito.when(transactionRepository.save(transaction)).thenReturn(transaction);
		assertThrows(InsufficientCashBalance.class, ()->transactionServiceImpl.buyShares(TransactionType.BUY, transactionDto));
	}
}
