package com.hcl.tradingapplication.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.tradingapplication.dto.ApiResponse;
import com.hcl.tradingapplication.dto.CashAccountRecord;
import com.hcl.tradingapplication.service.CashAccountSerevice;

@ExtendWith(SpringExtension.class)
 class CashAccountControllerTest {
	@Mock
	private CashAccountSerevice cashAccountService;

	@InjectMocks
	private CashAccountController cashAccountController;

	@Test
	void testUpdateBalanceSuccess() {
		CashAccountRecord cashAccountRecord = CashAccountRecord.builder().cashAccountId(1L).balance(100.0).build();

		when(cashAccountService.updateBalance(cashAccountRecord))
				.thenReturn(ApiResponse.builder().message("Success").httpStatus("OK").build());

		ResponseEntity<ApiResponse> responseEntity = cashAccountController.updateBalance(cashAccountRecord);

		assert responseEntity.getStatusCode() == HttpStatus.OK;
		assert responseEntity.getBody().message().equals("Success");
		assertNotNull(responseEntity);
	}
}
