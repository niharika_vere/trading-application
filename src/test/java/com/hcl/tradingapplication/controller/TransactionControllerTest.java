package com.hcl.tradingapplication.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.tradingapplication.dto.ApiResponse;
import com.hcl.tradingapplication.dto.TransactionDto;
import com.hcl.tradingapplication.entity.TransactionType;
import com.hcl.tradingapplication.service.TransactionService;

@ExtendWith(SpringExtension.class)
class TransactionControllerTest {
	@Mock
	private TransactionService transactionService;
	@InjectMocks
	private TransactionController transactionController;

	@Test
	void testPurchaseShare() {
		TransactionDto transactionDto = TransactionDto.builder().noOfShares(2).stockId(1l).userId(1l).build();
		ApiResponse apiResponse = ApiResponse.builder().httpStatus("4001").message("Success").build();
		Mockito.when(transactionService.buyShares(TransactionType.BUY, transactionDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity = transactionController.buyShares(TransactionType.BUY,
				transactionDto);
		assertNotNull(responseEntity);
		assertEquals("Success", responseEntity.getBody().message());
	}
}
