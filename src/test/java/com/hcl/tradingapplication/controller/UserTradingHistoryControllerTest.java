package com.hcl.tradingapplication.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.tradingapplication.dto.UserHistoryDto;
import com.hcl.tradingapplication.service.UserTradingHistoryService;

@ExtendWith(SpringExtension.class)
class UserTradingHistoryControllerTest {
	@Mock
	UserTradingHistoryService userTradingHistoryService;
	@InjectMocks
	UserTradingHistoryController userTradingHistoryController;

	@Test
	void testController() {
		UserHistoryDto userHistoryDto = UserHistoryDto.builder().cashBalance(123.0).build();
		Mockito.when(userTradingHistoryService.tradingHistory(1l)).thenReturn(userHistoryDto);
		ResponseEntity<UserHistoryDto> responseEntity = userTradingHistoryController.getUserTradingHistory(1l);
		assertNotNull(responseEntity);
		assertEquals(userHistoryDto.cashBalance(), responseEntity.getBody().cashBalance());
	}

}
