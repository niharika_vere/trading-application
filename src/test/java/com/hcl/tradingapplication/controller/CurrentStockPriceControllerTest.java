package com.hcl.tradingapplication.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.hcl.tradingapplication.dto.StockPriceRec;
import com.hcl.tradingapplication.exception.TradingNotStartedException;
import com.hcl.tradingapplication.service.CurrentStockPriceService;

@ExtendWith(MockitoExtension.class)
 class CurrentStockPriceControllerTest {
	@Mock
    private CurrentStockPriceService currentStockPriceService;
 
    @InjectMocks
    private CurrentStockPriceController currentStockPriceController;
 
    @Test
     void testGetCurrentPriceStockSuccess() {
      
        StockPriceRec expectedStockPriceRec = new StockPriceRec("ABC", 100.0, LocalDate.now());
        when(currentStockPriceService.getCurrentPriceStock(1L)).thenReturn(expectedStockPriceRec);
 
        StockPriceRec actualStockPriceRec = currentStockPriceController.getCurrentPriceStock(1L).getBody();
 
        assertEquals(expectedStockPriceRec, actualStockPriceRec);
    }
 

}
